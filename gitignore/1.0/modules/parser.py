
class Gitignore:
    class Section:
        """A Section in a gitignore file (starting with # <description>)"""
        def __init__(self, header="", body=None):
            if body is None:
                body = []
            self.header = header
            self.body = body

        def __getitem__(self, k):
            return self.body[k]

        def append(self, el):
            if el:
                self.body.append(el)

        def __str__(self):
            ret = ""
            if self.header:
                ret += self.header + "\n"
            ret += "\n".join(self.body)
            return ret

    def __init__(self, name="", sections=None):
        if sections is None:
            sections = []
        self.name = name
        self.sections = sections

    def __contains__(self, el):
        return any(map(lambda l: el in l, self.sections))
        # for sec in self.sections:
        #     if el in sec:
        #         return True
        # return False

    def add_section(self, sec: Section):
        # shortcuts
        if not sec.body:
            return
        if not self.sections:
            self.sections.append(sec)
            return

        # remove double entries
        newSec = Gitignore.Section(sec.header)
        for el in sec:
            if el in self:
                continue
            newSec.append(el)

        if newSec.body:
            self.sections.append(newSec)

    def __str__(self):
        ret = ""
        if self.name:
            ret = "# {1} {0} {1}\n\n".format(self.name, "=" * 10)
        ret += "\n\n".join(map(str, self.sections))
        return ret


def parse_files(files, path):
    import os.path
    ignores = []
    curIgnore = None
    for ignore_file in files:
        if curIgnore:
            ignores.append(curIgnore)

        curIgnore = Gitignore(ignore_file)
        curSec = Gitignore.Section()

        fpath = os.path.join(path, "{}.gitignore".format(ignore_file))
        with open(fpath, "r") as f:
            lines = [l.strip() for l in f.readlines()]

        for line in lines:
            if line.startswith("#"):
                curIgnore.add_section(curSec)
                curSec = Gitignore.Section(line)
            else:
                curSec.append(line)
                # if all([line not in ig for ig in ignores]):
                #     curSec.append(line)
        curIgnore.add_section(curSec)
    if curIgnore:
        ignores.append(curIgnore)
    return ignores


# manual testing
def main():
    import os.path
    data_dir = os.path.abspath(os.path.join(os.path.curdir, "data"))
    files = ["C", "C++"]
    ignores = parse_files(files, data_dir)
    for i in ignores:
        print(i)
        print()


if __name__ == "__main__":
    main()
